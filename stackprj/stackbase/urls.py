from django.urls import path
from . import views

app_name = 'stackbase'

urlpatterns = [
    path('', views.home, name="home"),
    path('about/', views.about, name="question-lists"),

    # CRUD Function
    path('question/', views.QuestionListView.as_view(), name="question_list"),
    path('question/new/', views.QuestionCreateView.as_view(), name="question_create"),
    path('question/<int:pk>/', views.QuestionDetailView.as_view(),
         name="question_detail"),
    path('question/<int:pk>/update/',
         views.QuestionUpdateView.as_view(), name="question_update"),
    path('question/<int:pk>/delete/',
         views.QuestionDeleteView.as_view(), name="question_delete"),
    path('question/<int:pk>/comment/',
         views.AddCommentView.as_view(), name="question_comment"),
    path('like/<int:pk>', views.like_view, name="like_post")
]
