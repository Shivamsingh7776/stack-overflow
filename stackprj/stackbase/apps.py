from django.apps import AppConfig


class StackbaseConfig(AppConfig):
    name = 'stackbase'
